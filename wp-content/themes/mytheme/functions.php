<?php

function load_css(){
    wp_register_style('bootstrap_css', get_template_directory_uri().'/assets/css/bootstrap.min.css',array(),'4.5.0');
    wp_enqueue_style('bootstrap_css');

    wp_register_style('main', get_template_directory_uri().'/assets/css/main.css',array());
    wp_enqueue_style('main');

}
add_action('wp_enqueue_scripts', 'load_css');

function load_js(){

    wp_enqueue_script('jquery');
    
    wp_register_script('bootstrap_js', get_template_directory_uri().'/assets/js/bootstrap.min.js', array('jquery'), '4.5.0', true);

    wp_enqueue_style('bootstrap_js');
}
add_action('wp_enqueue_scripts', 'load_js');

//THEME OPTIONS
add_theme_support('menus');
add_theme_support('post-thumbnails');
add_theme_support('widgets');


//MENUS
register_nav_menus(
    array(
        'top-menu' => 'Top Menu Location',
        'mobile-menu' => 'Mobile Menu Location',
    )
);

//Custome Image Sizes
add_image_size('blog-large', 800, 400, true);
add_image_size('blog-small', 300, 200, true);


//Register Sidebars
function my_sidebars(){
    register_sidebar(
        array(
                'name'=> 'Page Sidebar',
                'id'=> 'page-sidebar',
                'before_title'=> '<h4 class="widget-title">',
                'after_title'=> '</h4>'
            )
        );
}

add_action('widgets_init', 'my_sidebars');