<!DOCTYPE html>
<html lang='eng'>

<head>
    <meta charset='UFT-8'>
    <title>Document</title>
</head>

<body>

    <?php wp_head(); ?>

    <header>

        <div class="container-fluid">
            <div class="row">
                <div class="col12">
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'top_menu'
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
    </header>