const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');

function style(){
    return gulp.src('./assets/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'))
    .pipe(browserSync.stream());
}

function watch(){
    browserSync.init({
        proxy:"mysitewp.local/",
        notify: false
        
    });

    gulp.watch('./assets/sass/**/*.scss', style);
    gulp.watch('./**/*.php').on('change', browserSync.reload);
    gulp.watch('./assets/lib/js/**/*.js'). on('change', browserSync.reload);
}

exports.style = style;
exports.watch = watch;