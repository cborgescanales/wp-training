jQuery(function () {

    jQuery('.open-header-menu-left').click(function () {
        jQuery('.header-menu-left-container').removeClass('close-nav');
    });

    jQuery('.close-header-menu-left').click(function () {
        jQuery('.header-menu-left-container').addClass('close-nav');
    });

    jQuery('.clickable-li').click(function () {
        jQuery(this).next('div.submenu').toggleClass('close-nav');
        jQuery(this).toggleClass('arrow-class');
    });


    jQuery('.clickable-img').click(function () {
        var imgUrl = jQuery(this).attr('src');
        jQuery('#expandedImg').attr('src', imgUrl).load(function() {});
    });

    jQuery('.size-box').click(function(){
        jQuery('.selected-size').removeClass('selected-size');
        jQuery(this).addClass('selected-size');
    })

    jQuery('.open-body').click(function () {
        jQuery(this).next().toggleClass('no-display');
    });
});

