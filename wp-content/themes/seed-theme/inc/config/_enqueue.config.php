<?php

\add_filter('wp_enqueue_scripts', function() {
    wp_enqueue_script('misscherries/js/main', \get_stylesheet_directory_uri() . '/dist/js/main.min.js', array('jquery'), MissCherries_THEME_SCRIPTS_VERSION, true);
    wp_localize_script('misscherries/js/main', 'theme_data', array(
        'ajax_url' => admin_url('admin-ajax.php'),
    ));
});

\add_filter('wp_enqueue_scripts', function() {
    wp_enqueue_style('misscherries/css/main', \get_stylesheet_directory_uri() . '/dist/css/main.css', false, MissCherries_THEME_STYLES_VERSION);
});

