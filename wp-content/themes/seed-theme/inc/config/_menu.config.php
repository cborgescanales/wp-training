<?php


add_action( 'after_setup_theme', function () {
    register_nav_menus( array(
        'header_menu' => 'Header Menu',
        'header_menu_left' => 'Header Menu Left',
        'header_second_menu' => 'Header Second Menu',
        'footer_menu' => 'Footer  Menu',
    ));
});
