<?php
/**
 * Register a product post type.
 *
 *
 */

if ( !function_exists( 'create_posttype_product()' ) ) {
    function create_posttype_product() {
        $slug = 'mc-product';
        $labels = array(
            'name'               => _x( 'Product', 'post type general name', 'misscherries' ),
            'singular_name'      => _x( 'Product', 'post type singular name', 'misscherries' ),
            'menu_name'          => _x( 'Products', 'admin menu', 'misscherries' ),
            'name_admin_bar'     => _x( 'Product', 'add new on admin bar', 'misscherries' ),
            'add_new'            => _x( 'Add new product', $slug, 'misscherries' ),
            'add_new_item'       => __( 'Add new product', 'misscherries' ),
            'new_item'           => __( 'New product', 'misscherries' ),
            'edit_item'          => __( 'Edit product', 'misscherries' ),
            'view_item'          => __( 'View product', 'misscherries' ),
            'all_items'          => __( 'All product', 'misscherries' ),
            'search_items'       => __( 'Search product', 'misscherries' ),
            'parent_item_colon'  => __( 'Parent product:', 'misscherries' ),
            'not_found'          => __( 'Product not found.', 'misscherries' ),
            'not_found_in_trash' => __( 'Product not found on trash.', 'misscherries' )
        );

        $args = array(
            'labels'             => $labels,
            'description'        => __( 'Description.', 'misscherries' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            // 'rewrite'            => array('slug' => ''),
            'show_in_rest'       => true,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => 3,
            // 'menu_icon'          => 'dashicons-media-text',
            'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
        );

        register_post_type( $slug, $args );
    }
}
add_action( 'init', 'create_posttype_product' );
