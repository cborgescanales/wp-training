<?php

if (!function_exists('taxonomy_product_size_register')) {
    function taxonomy_product_size_register()
    {
        // Labels part for the GUI
        $labels = array(
            'name'                      => _x('Products Size', 'taxonomy general name', 'misscherries'),
            'singular_name'             => _x('Products Size', 'taxonomy singular name', 'misscherries'),
            'search_items'              => __('Search products size', 'misscherries'),
            'popular_items'             => __('Popular products size', 'misscherries'),
            'all_items'                 => __('All products sizes', 'misscherries'),
            'parent_item'               => __('Parent', 'misscherries'),
            'parent_item_colon'         => __('Parent:', 'misscherries'),
            'edit_item'                 => __('Edit products size', 'misscherries'),
            'update_item'               => __('Update products size', 'misscherries'),
            'add_new_item'              => __('Add new products size', 'misscherries'),
            'new_item_name'             => __('Format products size', 'misscherries'),
            'separate_items_with_commas' => __('Separate products size with commas', 'misscherries'),
            'add_or_remove_items'       => __('Add or remove products size', 'misscherries'),
            'choose_from_most_used'     => __('Choose from most used products size', 'misscherries'),
            'menu_name'                 => __('Products Size', 'misscherries'),
            'back_to_items'             => __('← Back to Products Size', 'misscherries'),
        );

        // Now register the non-hierarchical taxonomy like tag
        register_taxonomy('products-size', 'mc-product',  array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            // 'show_in_rest' => true,
            'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            //'rewrite' => array( 'slug' => 'country' ),
        ));
    }
}
add_action('init', 'taxonomy_product_size_register');
