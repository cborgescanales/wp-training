<?php

if (!function_exists('taxonomy_product_color_register')) {
    function taxonomy_product_color_register()
    {
        // Labels part for the GUI
        $labels = array(
            'name'                      => _x('Products Color', 'taxonomy general name', 'misscherries'),
            'singular_name'             => _x('Products Color', 'taxonomy singular name', 'misscherries'),
            'search_items'              => __('Search products color', 'misscherries'),
            'popular_items'             => __('Popular products color', 'misscherries'),
            'all_items'                 => __('All products sizes', 'misscherries'),
            'parent_item'               => __(null),
            'parent_item_colon'         => __(null),
            'edit_item'                 => __('Edit products color', 'misscherries'),
            'update_item'               => __('Update products color', 'misscherries'),
            'add_new_item'              => __('Add new products color', 'misscherries'),
            'new_item_name'             => __('Format products color', 'misscherries'),
            'separate_items_with_commas' => __('Separate products color with commas', 'misscherries'),
            'add_or_remove_items'       => __('Add or remove products color', 'misscherries'),
            'choose_from_most_used'     => __('Choose from most used products color', 'misscherries'),
            'menu_name'                 => __('Products Color', 'misscherries'),
            'back_to_items'             => __('← Back to Products Color', 'misscherries'),
        );

        // Now register the non-hierarchical taxonomy like tag
        register_taxonomy('products-color', 'mc-product',  array(
            'hierarchical' => false,
            'labels' => $labels,
            'show_ui' => true,
            // 'show_in_rest' => true,
            'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            //'rewrite' => array( 'slug' => 'country' ),
        ));
    }
}
add_action('init', 'taxonomy_product_color_register');
