<?php

if (!function_exists('taxonomy_product_category_register')) {
    function taxonomy_product_category_register()
    {
        // Labels part for the GUI
        $labels = array(
            'name'                      => _x('Products Category', 'taxonomy general name', 'misscherries'),
            'singular_name'             => _x('Products Category', 'taxonomy singular name', 'misscherries'),
            'search_items'              => __('Search products category', 'misscherries'),
            'popular_items'             => __('Popular products category', 'misscherries'),
            'all_items'                 => __('All products category', 'misscherries'),
            'parent_item'               => __('Parent', 'misscherries'),
            'parent_item_colon'         => __('Parent:', 'misscherries'),
            'edit_item'                 => __('Edit products category', 'misscherries'),
            'update_item'               => __('Update products category', 'misscherries'),
            'add_new_item'              => __('Add new products category', 'misscherries'),
            'new_item_name'             => __('Format products category', 'misscherries'),
            'separate_items_with_commas' => __('Separate products category with commas', 'misscherries'),
            'add_or_remove_items'       => __('Add or remove products category', 'misscherries'),
            'choose_from_most_used'     => __('Choose from most used products category', 'misscherries'),
            'menu_name'                 => __('Products Category', 'misscherries'),
            'back_to_items'             => __('← Back to Products Category', 'misscherries'),
        );

        // Now register the non-hierarchical taxonomy like tag
        register_taxonomy('products-category', 'mc-product',  array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            // 'show_in_rest' => true,
            'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            //'rewrite' => array( 'slug' => 'country' ),
        ));

        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_5f05c69d05f76',
                'title' => 'products taxonomy',
                'fields' => array(
                    array(
                        'key' => 'field_5f05c6a7512c1',
                        'label' => 'Image for Product',
                        'name' => 'category_product_img',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'url',
                        'preview_size' => 'medium',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                    array(
                        'key' => 'field_5f05c9e7e36b6',
                        'label' => 'Image for Product Mobile',
                        'name' => 'category_product_img_mobile',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'url',
                        'preview_size' => 'medium',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'taxonomy',
                            'operator' => '==',
                            'value' => 'products-category',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));

            endif;
    }
}
add_action('init', 'taxonomy_product_category_register');
