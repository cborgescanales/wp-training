<?php
function admin_post_list_add_export_button($which){
    global $typenow;

    if ('mc-product' === $typenow && 'top' === $which) {
?>
        <input type="submit" name="export_all_products" class="button button-primary" value="<?php _e('Export All Products'); ?>" />
<?php
    }
}

add_action('manage_posts_extra_tablenav', 'admin_post_list_add_export_button', 20, 1);

function func_export_all_posts()
{
    if (isset($_GET['export_all_products'])) {
        $arg = array(
            'post_type' => 'mc-product',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );

        global $post;
        $arr_post = get_posts($arg);
        if ($arr_post) {

            header('Content-type: text/csv');
            header('Content-Disposition: attachment; filename="wp-posts.csv"');
            header('Pragma: no-cache');
            header('Expires: 0');

            $file = fopen('php://output', 'w');

            fputcsv($file, array('Post Title', 'URL', 'Product Description'));

            // fputcsv($file, array('Post Title', 'URL', 'Categories', 'Tags'));

            foreach ($arr_post as $post) {
                setup_postdata($post);

                $description = get_field('product_description');

            //     $categories = get_the_category();
            //     $cats = array();
            //     if (!empty($categories)) {
            //         foreach ($categories as $category) {
            //             $cats[] = $category->name;
            //         }
            //     }

            //     $post_tags = get_the_tags();
            //     $tags = array();
            //     if (!empty($post_tags)) {
            //         foreach ($post_tags as $tag) {
            //             $tags[] = $tag->name;
            //         }
            //     }

            //     fputcsv($file, array(get_the_title(), get_the_permalink(), implode(",", $cats), implode(",", $tags)));
                    fputcsv($file, array(get_the_title(), get_the_permalink(), $description));
             }

            exit();
        }
    }
}

add_action('init', 'func_export_all_posts');
