<?php

use \Brooktec\Helpers\Assets as Assets;

$args = array(
    'taxonomy' => 'products-category',
    'orderby' => 'name',
    'parent' => 0,
    'hide_empty' => 0,

);
$categories = get_categories($args);


?>

<div class="col-2">
    <div class="header-dropdown-menu">
        <button class="btn open-header-menu-left"><i class="fa fa-bars" aria-hidden="true"></i></button>


        <nav class="header-menu-left-container close-nav">

            <button class="btn close-header-menu-left"><img src="<?php echo Assets::getUri('close-symbol.png'); ?>"></button>

            <div class="header-menu-left-nav">
                <?php

                foreach ($categories as $cat) {
                ?>
                    <div class="item ">
                        <button class="clickable-li arrow-class">
                            <?php echo $cat->name; ?>
                        </button>

                        <?php $cat_children = get_terms('products-category', array('child_of' => $cat->term_id, 'orderby' => 'name', 'hide_empty' => 0));

                        if ($cat_children) {
                        ?>
                            <div class="submenu close-nav">
                                <?php
                                foreach ($cat_children as $cat_child) {
                                ?>

                                    <a class="sub-item" href="<?php echo get_tag_link($cat_child->term_id); ?>"><?php echo $cat_child->name; ?></a>

                                <?php
                                } ?>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                <?php
                } ?>

            </div>

            <hr class="nav-hr">

            <?php
            wp_nav_menu(array(
                'theme_location' => 'header_menu_left',
                'container_class' => 'second-menu'
            ));
            ?>

        </nav>
    </div>
</div>
