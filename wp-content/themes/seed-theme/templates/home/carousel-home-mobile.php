<?php

use \Brooktec\Helpers\Assets as Assets;

$args = array(
    'taxonomy' => 'products-category',
    'orderby' => 'name',
    'parent' =>0,
    'hide_empty' => 0,

);
$categories = get_categories($args);

?>

<!-- <div class="row ">
    <div class="col12"> -->
        <div id="main-carousel-mobile" class="carousel slide carousel-mobile">
            <ol id="main-carousel-indicators" class="carousel-indicators">
                <li data-target="#main-carousel-mobile" data-slide-to="0" class="active "></li>
                <li data-target="#main-carousel-mobile" data-slide-to="1"></li>
                <li data-target="#main-carousel-mobile" data-slide-to="2" ></li>
            </ol>
            <div class="carousel-inner">

                <?php
                $endFirstcontainer = 3;
                $startSecondcontainer = 4;
                $endSecondcontainer = 7;
                $counter = 0;
                foreach ($categories as $cat) {
                    $format_img_mobile = get_field('category_product_img_mobile', $cat);

                    if ($counter == 0 || $counter == $startSecondcontainer) {
                        echo '
                            <div class="carousel-item' . (($counter == 0) ? ' active' : '') . '">
                                <div class="container d-block w-100">
                                     <div class="row">';
                    }
                ?>

                    <div class="col-6">
                        <div class="card-carousel">
                            <div class="card-img-top-carousel" style="background-image: url('<?php echo $format_img_mobile; ?>')"></div>

                            <div class="black-container-carousel">
                                <p class="card-text"><?php echo $cat->name; ?></p>

                            </div>
                        </div>
                    </div>



                <?php
                    if ($counter == $endFirstcontainer || $counter == $endSecondcontainer) {
                        echo '    </div>
                                </div>
                            </div>';
                    }
                    $counter++;
                }
                ?>

                <div class="carousel-item">
                    <div class="d-flex w-100 ">
                        <div class="carousel-lastimg" style="background-image:url('<?php echo Assets::getUri('home-girl-mobile.png'); ?>')" ></div>
                        <div class="carousel-lastimg-logo" style="background-image:url('<?php echo Assets::getUri('home-girl-logo-mobile.png'); ?>')" ></div>
                        <div class="carousel-lastimg-text" style="background-image:url('<?php echo Assets::getUri('home-girl-text-mobile.png'); ?>')" ></div>
                    </div>
                </div>
            </div>
        </div>
