<?php

use \Brooktec\Helpers\Assets as Assets;

$args = array(
    'taxonomy' => 'products-category',
    'orderby' => 'name',
    'parent' =>0,
    'hide_empty' => 0,

);
$categories = get_categories($args);

?>

<section class="accessories" style="background-image: url(<?php echo Assets::getUri('home-back-accesories.png'); ?>)">

    <div class="red-flower" style="background-image: url(<?php echo Assets::getUri('red-flower.png'); ?>)"> </div>

        <div class="black-shape" style="background-image: url(<?php echo Assets::getUri('black-lines.png'); ?>)"></div>

        <div class="brown-shape" style="background-image: url(<?php echo Assets::getUri('brown-shape.png'); ?>)"> </div>

    <div class="container">

        <div class="row text-center">



            <?php

            $counter = 0;
            $num = 8;
            foreach ($categories as $cat) {
                $format_img = get_field('category_product_img', $cat);
                $format_img_mobile = get_field('category_product_img_mobile', $cat);
            ?>

                <div class="col-6 col-md-4 <?php if ($counter == ($num - 2)) {echo ' offset-md-4'; } ?>">
                    <div class="card-accessories card<?php  if ($counter == ($num-1)) {echo ' last-row-card';} ?> ">
                        <div class="card-img-top" style="background-image: url(<?php echo  $format_img ?>)"></div>
                        <div class="card-body  card-body-accessories">
                            <div class=" accessories-black-container">
                                <p class="card-text card-text-accessories"> <?php echo $cat->name ?> </p>
                            </div>
                        </div>
                    </div>
                </div>


            <?php
                $counter++;
            }
            ?>

        </div>
    </div>
</section>
