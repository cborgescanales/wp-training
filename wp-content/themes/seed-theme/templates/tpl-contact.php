<?php
/*
Template Name: contact us
*/

?>

<?php get_header(); ?>
<div class="section-contact">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 offset-md-4'">

                <div class="title"><?php _e('CONTACTO', 'misscherries') ?></div>
                <div class="container-general-info">
                    <div class="subtitle"><?php _e('ATENCIÓN AL CLIENTE', 'misscherries') ?></div>
                    <div class="email-contact"><?php the_field('contact_us_email') ?></div>
                </div>

                <div class="container-form">
                    <div class="title-form"><?php _e('¿NECESITAS AYUDA?', 'mischerries') ?></div>
                    <?php echo do_shortcode('[contact-form-7 id="149" title="Contact Us Form"]'); ?>
                </div>

            </div>
        </div>
    </div>
</div>





<?php get_footer(); ?>
