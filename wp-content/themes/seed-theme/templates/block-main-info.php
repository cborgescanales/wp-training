<?php

use \Brooktec\Helpers\Assets as Assets;

?>

<!-- <section class="socialclub">
    <div class="container-fluid ">
        <div class="row justify-content-around">
            <div class="col-12 order-md-last bck-color-col"> -->

                <div class="about-us-container ">

                    <div class="about-us-title">
                        <?php
                        the_field('block_main_about_us_text', 'option');
                        ?>
                    </div>

                    <div class="about-us-list">
                        <span class="about-us-list-text ">
                            <?php
                            the_field('general_condition_one', 'option');
                            ?>
                        </span>

                        <span class="about-us-list-text-two">
                            <?php
                            the_field('general_condition_two', 'option');
                            ?>
                        </span>

                        <span class="about-us-list-text-three">
                            <?php
                            the_field('general_condition_three', 'option');
                            ?>
                        </span>
                    </div>
                </div>

            <!-- </div> -->
