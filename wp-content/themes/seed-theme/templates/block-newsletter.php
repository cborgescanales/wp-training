<?php

use \Brooktec\Helpers\Assets as Assets;

?>
            <!-- <div class=" col-11 col-md-12 order-md-first"> -->
                <div class="socialclub-container">
                    <div class="socialclub-title ">
                        <?php
                        the_field('block_main_social_club_title', 'option');
                        ?>
                    </div>
                    <div class="socialclub-text-container ">
                        <span class="socialclub-text">
                            <?php
                            the_field('block_main_social_club', 'option');
                            ?>
                        </span>

                        <span class="socialclub-lastline">
                            <?php
                            the_field('block_main_social_club_foot', 'option');
                            ?>
                        </span>

                    </div>

                    <hr class="socialclub-custom-hr">
                    <button type='button' class='btn btn-join'><?php _e('ME APUNTO', 'misscherries'); ?></button>

               </div>
           <!--   </div>
        </div>
    </div>


</section> -->

