<?php

use \Brooktec\Helpers\Assets as Assets;

?>

<section class="instagram-block" style="background-image:url('<?php echo Assets::getUri('home-about-us-bckimg.png'); ?>')">

    <div class="container-fluid">
        <div class="row text-center">
            <div class="col">

                <div class="instagram-block-container">

                    <span class=" hastag-box">
                        <div class="hastag-box-text font-12">
                            <?php
                            _e('#MISSCHERRIES', 'misscherries')
                            ?>
                        </div>
                    </span>

                    <div class="footer-main-pic">

                        <div class="blue-lines" style="background-img:url('<?php echo Assets::getUri('blue-lines.png'); ?>)'"> </div>


                        <img class="instagram-block-img" src="<?php echo Assets::getUri('home-about-us.png'); ?>">
                        <img class="instagram-block-mobile-img" src="<?php echo Assets::getUri('home-about-us-mobile.png'); ?>">

                    </div>
                    <div class="follow-us-text">
                        <?php
                        _e('SÍGUENOS', 'misscherries')
                        ?>
                    </div>
                    <div class="instagram-missc">
                        <span class="fab fa-instagram instagram-picture"></span>
                        <?php
                        _e('@MISSCHERRIES', 'misscherries')

                        ?>
                    </div>

                </div>
            </div>
        </div>

</section>
