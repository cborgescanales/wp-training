<?php

/**
 * Header Menu
 */

use \Brooktec\Helpers\Assets as Assets;

$args = array(
    'taxonomy' => 'products-category',
    'orderby' => 'name',
    'parent' => 0,
    'hide_empty' => 0,

);
$categories = get_categories($args);

$languages = apply_filters('wpml_active_languages', NULL, 'skip_missing=0&orderby=code');

?>

<div class="header-menu-box" id="header-menu">
    <div class="container-fluid">
        <div class="row">

            <?php get_template_part('templates/header', 'dropdown-menu'); ?>

            <div class="col text-center align-self-center">
                <div class="logo-container">
                    <a href="<?php echo site_url(); ?>"><img class="misscherries-logo" src="<?php echo Assets::getUri('misscherries-logo.png'); ?>"></a>
                </div>
            </div>

            <div class="col-3">
                <div class="header-menu-container">

                    <?php
                    if (!empty($languages)) {
                    ?>
                        <div id="header_language_list" class="btn-group header-language-menu">
                            <?php
                            foreach ($languages as $l) {

                                if ($l['active']) { ?>
                                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                        <?php
                                        echo apply_filters('wpml_display_language_names', NULL, $l['native_name'], $l['translated_name']);

                                        ?>
                                    </button>
                                <?php
                                } else { ?>

                                    <div class="dropdown-menu">
                                        <a href="<?php echo $l['url'] ?>">
                                            <?php echo apply_filters('wpml_display_language_names', NULL, $l['native_name'], $l['translated_name']); ?>
                                        </a>
                                    </div>

                            <?php }
                            } ?>

                        </div>

                    <?php } ?>

                    <div class="header-menu-icons-container">

                        <span class="fa fa-heart"></span>
                        <span class="fas fa-shopping-bag"></span>
                    </div>

                    <div class="header-menu-my-account">
                        <a href="#"><?php _e("MI CUENTA") ?></a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="header-second-menu"> -->
<div class="container">
    <div class="row">
        <div class="col">
            <nav class="header-second-menu">
                <ul class="header-second-menu-ul">
                    <?php
                    foreach ($categories as $cat) {
                    ?>

                        <li class="menu-item">
                            <a href="<?php echo get_tag_link($cat->term_id); ?>">
                                <?php echo $cat->name; ?>
                            </a>
                        </li>


                    <?php }  ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- </div> -->
