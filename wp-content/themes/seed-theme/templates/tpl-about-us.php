<?php
/*
Template Name: about us
*/

?>

<?php get_header(); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col12">
            <div class="about-us-container">
                <div class="title">
                    <?php
                    _e('QUIÉNES SOMOS', 'misscherries')
                    ?>
                </div>

                <div class="col-6">
                    <div class="presentation-text">
                        <?php
                        the_field('about_us_presentation_text');
                        ?>
                    </div>
                </div>

                <?php
                $image = get_field('about_us_main_img');
                if ($image) :
                    $url = $image['url'];
                    $alt = $image['alt'];
                ?>
                    <div class="about-us-img">

                        <img class="main-img" src="<?php echo esc_url($url); ?>" alt="<?php echo esc_attr($alt); ?> ">

                    </div>
                <?php endif; ?>

                <div class="col-6">
                    <div class="main-text">
                        <?php
                        the_field('about_us_main_text');
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
