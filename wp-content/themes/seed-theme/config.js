module.exports = {
    "environment": process.env.NODE_ENV || "production",
    "imgPath": "./src/img",
    "fontPath": "./src/fonts",
    "jsPath": "./src/js",
    "jsAdminPath": "./src/js-admin",
    "sassPath": "./src/sass",
    "vendorPath": "./src/vendor",
    "destPath": "./dist",
    "devUrl": process.env.DEVELOPMENT_URL || "",
    "vendor": {
        "sass": [
            './node_modules/@fortawesome/fontawesome-free/scss',
            './node_modules/bootstrap/scss'
        ],
        "css": [],
        "js": [
            './node_modules/bootstrap/dist/js/bootstrap.min.js'
        ],
        "fonts": [
            './node_modules/@fortawesome/fontawesome-free/webfonts/*'
        ]
    },
    "rsync":  {
        "pre": {
            "active": process.env.RSYNC_PRE_ACTIVE === '1' || false,
            "username": process.env.RSYNC_PRE_USERNAME || '',
            "server": process.env.RSYNC_PRE_SERVER || '',
            "destPath": process.env.RSYNC_PRE_DESTINATION_PATH || '',
            "pem": process.env.RSYNC_PRE_PEM || false
        },
        "pro": {
            "active": process.env.RSYNC_PRO_ACTIVE === '1' || false,
            "username": process.env.RSYNC_PRO_USERNAME || '',
            "server": process.env.RSYNC_PRO_SERVER || '',
            "destPath": process.env.RSYNC_PRO_DESTINATION_PATH || '',
            "pem": process.env.RSYNC_PRO_PEM || false
        }
    }
};
