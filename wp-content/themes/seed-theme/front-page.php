<?php

use \Brooktec\Helpers\Assets as Assets;

$user_image = apply_filters('brooktec/apiuser/user/get/image', (get_avatar_url(wp_get_current_user()) ?: ''));
?>


<?php get_header(); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col">

            <div class="div-fake-carousel">
                <img class="fake-carousel-img" src="<?php echo Assets::getUri('fake-carousel.png'); ?>">
                <img class="carousel-message" src="<?php echo Assets::getUri('home-carousel-message.png'); ?>">
            </div>

            <div class="home-mobile-plant-img">
                <a href="<?php echo site_url(); ?>"><img class="img-fluid" src="<?php echo Assets::getUri('home-plant-mobile.png'); ?>"></a>
                <span class="box-top-sales">
                    <p class="box-top-sales-text"><?php _e('TOP VENTAS', 'misscherries') ?></p>
                </span>
            </div>

        </div>
    </div>
</div>

<?php get_template_part('templates/home/carousel', 'home-mobile'); ?>
<?php get_template_part('templates/home/accessories', 'block-home'); ?>


<?php get_footer(); ?>
