<?php

/**
 * Footer
 */
?>

<?php

use \Brooktec\Helpers\Assets as Assets;

$user_image = apply_filters('brooktec/apiuser/user/get/image', (get_avatar_url(wp_get_current_user()) ?: ''));
?>

<section class="socialclub">
    <div class="container-fluid ">
        <div class="row justify-content-around">
            <div class="col-12 order-md-last bck-color-col">
                <?php get_template_part('templates/block', 'main-info'); ?>
            </div>

            <div class=" col-11 col-md-12 order-md-first">
                <?php get_template_part('templates/block', 'newsletter'); ?>
            </div>

        </div>
    </div>
</section>

<?php get_template_part('templates/block', 'inst-menu'); ?>


<footer class="footer-standard">
    <div class="container-footer-nav">
        <div class="dinamic-container-footer-nav">

            <div class="container">
                <div class="row ">
                    <div class="col">
                        <?php if (has_nav_menu('footer_menu')) {
                            wp_nav_menu(array(
                                'theme_location' => 'footer_menu',
                                'menu_class' => 'misscherries-footer-menu',
                            ));

                            $languages = apply_filters('wpml_active_languages', NULL, 'skip_missing=0&orderby=code');
                            if (!empty($languages)) {
                        ?>
                                <div id="footer_language_list" class="foot-language-list">
                                    <ul class="footer-language-ul">
                                        <?php foreach ($languages as $l) {
                                        ?>
                                            <li>
                                                <?php if ($l['active']) ?> <span class="active">
                                                    <?php if (!$l['active']) ?><a href="<?php $l['url'] ?>">
                                                        <?php echo apply_filters('wpml_display_language_names', NULL, $l['native_name'], $l['translated_name']);
                                                        if (!$l['active']) ?></a>
                                                    <?php if ($l['active']) ?></span>
                                            </li>
                                    <?php
                                        }
                                    }
                                    ?>

                                    </ul>
                                </div>

                            <?php  } ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row ">
                <div class="col-12">
                    <div class="footer-list-media">
                        <img class="footer-media-img-mobile" src="<?php echo Assets::getUri('social-media-icons-mobile.png'); ?>">
                        <span class="fab fa-instagram fa-2x color-socialmedia-footer"></span>
                        <span class="fab fa-facebook fa-2x color-socialmedia-footer"></span>
                        <span class="fab fa-pinterest-square fa-2x color-socialmedia-footer"></span>
                    </div>
                </div>
            </div>
        </div>


    </div>
</footer>

<div id="wp-scripts">
    <?php wp_footer(); ?>
</div>
</body>

</html>
