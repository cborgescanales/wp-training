<?php

use \Brooktec\Helpers\Assets as Assets;

$slug_product = $_GET['products-category'];

$args = array(
    // 'name'=>$slug_product,
    // 'post_type' => 'mc-product',
    // 'post_status' => 'publish',
    // 'numberposts' => 1

    //working
    'post_type' => 'mc-product',
    'post_status' => 'publish',
    'cat' => 'product_cat_rings'
);

$argssize = array(
    'taxonomy' => 'products-size',
    'orderby' => 'id',
    'parent' => 0,
    'hide_empty' => 0,

);
$sizes = get_categories($argssize);

$argscolor = array(
    'taxonomy' => 'products-color',
    'orderby' => 'id',
    'parent' => 0,
    'hide_empty' => 0,

);
$colors = get_categories($argscolor);

$argscat = array(
    'taxonomy' => 'products-category',
    'orderby' => 'rand',
    'parent' => 13,
    'hide_empty' => 0,
    'number'=>3,
    'exclude'=>28

);
$ring_categories = get_categories($argscat);

get_header();

$loop = new WP_Query($args);

if ($loop->have_posts()) :

    while ($loop->have_posts()) : $loop->the_post(); ?>
        <div class="title">Anillos - <strong><?php print the_title(); ?></strong> </div>

        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-4 no-display-mobile">
                    <div class="img-container">
                        <div class="tab-column">
                            <div class="tab-row">
                                <?php
                                $image1 = get_field('product_picture_one');
                                if ($image1) :
                                    $url1 = $image1['url'];
                                    $alt1 = $image1['alt'];
                                ?>
                                    <img class="clickable-img" src="<?php echo esc_url($url1); ?>" alt="<?php echo esc_attr($alt1); ?>">
                                <?php endif; ?>
                            </div>


                            <div class="tab-row">
                                <?php
                                $image2 = get_field('product_picture_two');
                                if ($image2) :
                                    $url2 = $image2['url'];
                                    $alt2 = $image2['alt'];
                                ?>
                                    <img class="clickable-img" src="<?php echo esc_url($url2); ?>" alt="<?php echo esc_attr($alt2); ?>">
                                <?php endif; ?>
                            </div>


                            <div class="tab-row">
                                <?php
                                $image3 = get_field('product_picture_three');
                                if ($image3) :
                                    $url3 = $image3['url'];
                                    $alt3 = $image3['alt'];
                                ?>
                                    <img class="clickable-img" src="<?php echo esc_url($url3); ?>" alt="<?php echo esc_attr($alt3); ?>">
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-4 no-display-mobile">
                    <div class="expand-img-container">
                        <img class="expanded-img" id="expandedImg" src="<?php echo esc_url($url2); ?>">
                        <div class="decorative-down-image" style="background-image:url('<?php echo Assets::getUri('white-heart.png'); ?>')"></div>
                    </div>

                </div>

                <div class="col-12 no-display-desktop">
                    <div id="carouselProductIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselProductIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselProductIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselProductIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="<?php echo esc_url($url1); ?>"   alt="<?php echo esc_attr($alt1); ?>">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="<?php echo esc_url($url2); ?>"   alt="<?php echo esc_attr($alt2); ?>">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="<?php echo esc_url($url3); ?>"  alt="<?php echo esc_attr($alt3); ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="left-col-container">
                        <div class="name-product"><?php print the_title(); ?></div>

                        <div class="price-container">
                            <div class="price"><?php the_field('product_price'); ?>€</div>
                            <div class="sales-price"><?php the_field('product_sales_price'); ?>€</div>
                        </div>

                        <div class="size-title"><?php _e('TALLA', 'misscherries'); ?> </div>
                        <div class="size-container">
                            <?php
                            foreach ($sizes as $size) {
                            ?>
                                <button class="btn size-box"><?php echo $size->name; ?></button>
                            <?php } ?>

                            <button class="btn last-size-box"><?php _e("GUÍA DE TALLAS", "mischerries"); ?></button>
                        </div>

                        <div class="color-container">
                            <div class="color-title"><?php _e("MATERIAL - COLOR", "misscherries"); ?></div>
                            <select name="select-color" id="select-color" class="color-select">
                                <?php
                                foreach ($colors as $color) {
                                ?>
                                    <option value="<?php echo $color->name; ?>"><?php echo $color->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <button class="btn btn-add-to-cart"><?php _e("AÑADIR AL CARRITO", "misscherries") ?></button>
                        <div class="bellow-btn-message"><?php the_field('general_condition_two', 'option'); ?></div>
                        <div class="container-product-information">
                            <div class="description">
                                <div class="description-title open-body"><?php _e('DESCRIPCIÓN', 'misscherries'); ?></div>
                                <div class="description-body no-display"><?php $description = get_field('product_description'); if ($description) :
                                   echo $description; else :_e('Información no disponible', 'misscherries'); endif; ?></div>
                                <hr>
                            </div>

                            <div class="materials">
                                <div class="materials-title open-body"><?php _e('MATERIALES Y ACABADOS', 'misscherries'); ?></div>
                                <div class="materials-body no-display"><?php $material = get_field('product_material_finishing_touch'); if ($material) :
                                   echo $material; else :_e('Información no disponible', 'misscherries'); endif; ?></div>
                                <hr>
                            </div>
                            <div class="delivery-conditions">
                                <div class="delivery-title open-body"><?php _e('ENVÍO Y DEVOLUCIONES', 'misscherries'); ?></div>
                                <div class="delivery-body no-display"><?php $delivery = get_field(''); if ($delivery) :
                                   echo $delivery; else :_e('Información no disponible', 'misscherries'); endif; ?></div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
    endwhile;

else :
    ?>

    <div>Product not available now</div>
<?php endif;
wp_reset_postdata(); ?>


<div class="container">
    <div class="row justify-content-center">
        <div class="col12">
            <div class="title-block"><?php _e("OTRAS SUGERENCIAS", "misscherries"); ?></div>
        </div>
    </div>
    <div class="row">

        <?php
        foreach ($ring_categories as $ring) {
        ?>
            <div class="col-4">
                <!-- <div class="img-product" style="background-image:url('<?php echo Assets::getUri('cat-img-card.png'); ?>)'"> </div> -->
                <img class="other-product-img" src="<?php echo Assets::getUri('cat-img-card.png'); ?>">
                <div class="other-product-name"><?php echo $ring->name; ?></div>
                <div class="other-product-price">0.0€</div>
            </div>
        <?php } ?>

    </div>
</div>


<?php get_footer(); ?>
